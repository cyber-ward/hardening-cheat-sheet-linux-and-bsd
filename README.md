# Hardening Cheat Sheet Linux and BSD
(NOTFINISHED)
FreeBSD hardening quick and dirty CheatSheet

Securing freebsd and other variant
First of all you should always consider partitioning. This way you can set partition permissions that will greatly help such as 
noexec, nosuid on /tmp list goes on. I will eventually go in depth  about this.
System hardening is the process of adding new defenses and removing weak spots in existing defenses. 
Honestly operating system default are horribad.
so we remove these weak ass setting and tweak detection of vulnerabilities, and adjusting weak configuration defaults.

Using Ed25519 for OpenSSH keys (instead of DSA/RSA/ECDSA)
Granting temporary access to your servers (using signed SSH keys)
2-step auth in ssh 
Deploy Opvpn config 
AllowUsers
AllowGroups I always suggest to create one specific login group, chroot it and remove most of it rights
DenyUsers
DenyGroups
Chroot the login users
Minimizing your resources
Adding wrapper
ingress traffic filtering 
Egress traffic

cgroups
namespaces
seccomp
port filtering
read-only file system



sudo pkg update && sudo pkg upgrade -f
sudo portsnap fetch
sudo portsnap extract


Turning FreeBSD to HardenedBSD
In this tutorial I show you how to turn FreeBSD 11.0-RELEASE into HardenedBSD’s 11-STABLE build that ships with LibreSSL.

Ingredients:
Recent FreeBSD installation
unbound
hbsd-update (from Github)
HardenedBSD update certificates (from Github)
Preparation
Make sure you are running a supported FreeBSD version. Otherwise, bad things will happen. Now is also the time to do backups (and by this I mean not just a recursive zfs snapshot but actual backups).

In a first step, we will install unbound and ca_root_nss. The former, because the current maintainer of base unbound is not willing to provide an easy way of doing DNSSEC validation without additional steps; the latter, because it eliminates HTTPS verification errors with fetch(1) later in this tutorial.

     pkg install unbound ca_root_nss
In a next step, we download the latest hbsd-update shellscript from HardenedBSD’s git repository, the corresponding config file, and certificates. Once fetched into a temporary folder, we modify the hbsd-update script to take into account the ports unbound install, and move the files into their final destination.

    % cd /tmp
    % fetch https://raw.githubusercontent.com/HardenedBSD/hardenedBSD/hardened/11-stable/master-libressl/{usr.sbin/hbsd-update/hbsd-update,etc/hbsd-update.conf,share/keys/hbsd-update/trusted/ca.hardenedbsd.org,share/keys/hbsd-update/trusted/dnssec.key-2016-07-30}
    % sed -i -e '/^UNBOUND_HOST=/ s/\/usr\/sbin\/unbound-host/\/usr\/local\/sbin\/unbound-host/' hbsd-update
    % chmod +x hbsd-update

    % sudo -i

    # mv hbsd-update /usr/sbin
    # mv hbsd-update.conf /etc
    # mkdir -p /usr/share/keys/hbsd-update/trusted
    # mv ca.hardenedbsd.org /usr/share/keys/hbsd-update/trusted
    # mv dnssec.key-2016-07-30 /usr/share/keys/hbsd-update/trusted
    # ln -s /usr/share/keys/hbsd-update/trusted/ca.hardenedbsd.org /usr/share/keys/hbsd-update/trusted/5905e1b4.0
    # ln -s /usr/share/keys/hbsd-update/trusted/dnssec.key-2016-07-30 /usr/share/keys/hbsd-update/trusted/dnssec.key
Now with the preparation out of the way, we can get to the meat:

Deploying HardenedBSD
You may have already guessed: we will use hbsd-update to turn FreeBSD into HardenedBSD and reboot once that’s done.

    # hbsd-update && reboot
NB: If you’ve checked out the FreeBSD source code to /usr/src, append the -s flag to have hbsd-update extract the HardenedBSD sources in-place. Do make sure to remove corresponding vcs directories like .svn or .git.

    # rm -rf /usr/src/{.svn,.git,*}
    # hbsd-update -s && reboot
Finally, you’ll notice that many of your installed third-party tools are now perfectly unusable, having been compiled for OpenSSL while we’re using LibreSSL. You’ll start fixing this by reinstalling pkg, and upgrading every installed package.

    # pkg-static install pkg
    # pkg upgrade -f
You’ll also find that this leads to the caveat that all the packages you have installed from ports are now missing shared libraries, and need to be reinstalled.

If you’ve installed any of those third-party tools from ports, you will want to reinstall them using the HardenedBSD ports tree.

To make full use of HardenedBSD’s security features, install secadm and secadm-kmod, and take a look at secadm-rules the HBSD project provides:

    % git clone https://github.com/hardenedbsd/hardenedbsd-ports ports
    % make -C ports/hardenedbsd/secadm config install clean
    % make -C ports/hardenedbsd/secadm-kmod config install clean
    % git clone https://github.com/HardenedBSD/secadm-rules


Now harden the system 
in /etc/sysctl.conf
kern.securelevel=2
security.bsd.see_other_uids=0
security.bsd.stack_guard_page=1
sysctl -w net.inet.icmp.bmcastecho=0
#
#      Disable ICMP routing redirects.  This could allow the computer to have its routing table corrupted by an
#      attacker.
#
sysctl -w net.inet.ip.redirect=0
sysctl -w net.inet.ip6.redirect=0
#
#     Disable ICMP broadcast probes.  This could allow an attacker to reverse engineer details of your
#     network infrastructure.
#
sysctl -w net.inet.icmp.maskrepl=0
#
#     Disable IP source routing.  This could allow attackers to spoof IP addresses that you normally trust as
#     internal hosts.
#
sysctl -w net.inet.ip.sourceroute=0
sysctl -w net.inet.ip.accept_sourceroute=0

#     Disable users from having access to configuration files.
#
chmod o= /etc/fstab
chmod o= /etc/ftpusers
chmod o= /etc/group
chmod o= /etc/hosts
chmod o= /etc/hosts.allow
chmod o= /etc/hosts.equiv
chmod o= /etc/hosts.lpd
chmod o= /etc/inetd.conf
chmod 	o= /etc/login.access
chmod o= /etc/login.conf
chmod o= /etc/newsyslog.conf
chmod o= /etc/rc.conf
chmod o= /etc/ssh/sshd_config
chmod o= /etc/sysctl.conf
chmod o= /etc/syslog.conf
chmod o= /etc/ttys
#
##################################################################
##################################################################
#
#      Enable root as the only account with the ability to schedule jobs.
#
echo "root" > /var/cron/allow
echo "root" > /var/at/at.allow
chmod o= /etc/crontab
chmod o= /usr/bin/crontab
chmod o= /usr/bin/at
chmod o= /usr/bin/atq
chmod o= /usr/bin/atrm
chmod o= /usr/bin/batch
#
##################################################################
##################################################################
#
#     Secure the root directory contents to prevent viewing.
#
chmod 710 /root
#
##################################################################
##################################################################
#
#     Disable user from having access to the system log file directory.
#
chmod o= /var/log
#

##################################################################
##################################################################
#
#     Merge all temporary file directories.
#
#     A single directory should be used for temporary files, not two.
#     The /var/tmp directory will be replaced with a link to /tmp.
#
#     The contents of the /var/tmp directory remain after a reboot.  The contents of the /tmp directory do not.
#
mv /var/tmp/* /tmp/
rm -rf /var/tmp
ln -s /tmp /var/tmp
#
echo 'syslogd_flags="-ss"' >> /etc/rc.conf
#
#      ICMP Redirect messages can be used by attackers to redirect traffic and should be ignored.
#
echo 'icmp_drop_redirect="YES"' >> /etc/rc.conf
#
#      sendmail is an insecure service and should be disabled.
#
echo 'sendmail_enable="NO"' >> /etc/rc.conf
#
#       The Internet Super Server (inetd) allows a number of simple Internet services to be enabled, including
#       finger, ftp ssh, and telnetd.  Enabling these services may increase risk of security problems by
#       increasing the exposure of your system.
#
echo 'inetd_enable="NO"' >> /etc/rc.conf

echo 'icmp_drop_redirect="YES"' >> /etc/rc.conf
#
#      sendmail is an insecure service and should be disabled.
#
echo 'sendmail_enable="NO"' >> /etc/rc.conf
#
#       The Internet Super Server (inetd) allows a number of simple Internet services to be enabled, including
#       finger, ftp ssh, and telnetd.  Enabling these services may increase risk of security problems by
#       increasing the exposure of your system.
#
echo 'inetd_enable="NO"' >> /etc/rc.conf
#
#      Network File System allows a system to share directories and files with other computers over a network
#      and should be disabled.
#
echo 'nfs_server_enable="NO"' >> /etc/rc.conf
#
echo 'nfs_client_enable="NO"' >> /etc/rc.conf
#
#
#      Network File System allows a system to share directories and files with other computers over a network
#      and should be disabled.
#
echo 'nfs_server_enable="NO"' >> /etc/rc.conf
#
echo 'nfs_client_enable="NO"' >> /etc/rc.conf
#
#      SSHD is a family of applications that can used with network connectivity tools.
#      This disables rlogin, RSH, RCP and telenet.
#
echo 'sshd_enable="NO"' >> /etc/rc.conf
#
#      Disable portmap if you are not running Network File Systems.
#
echo 'portmap_enable="NO"' >> /etc/rc.conf
#
#      Disable computer system details from being added to /etc/motd on system reboot.
#
echo 'update_motd="NO"' >> /etc/rc.conf
#
#      The /tmp directory should be cleared at startup to ensure that any malicious code that may have
#      entered into the temp file is removed.
#
echo 'clear_tmp_enable="YES"' >> /etc/rc.conf

sysctl -w net.inet.icmp.bmcastecho=0
#
#      Disable ICMP routing redirects.  This could allow the computer to have its routing table corrupted by an
#      attacker.
#
sysctl -w net.inet.ip.redirect=0
sysctl -w net.inet.ip6.redirect=0
#
#     Disable ICMP broadcast probes.  This could allow an attacker to reverse engineer details of your
#     network infrastructure.
#
sysctl -w net.inet.icmp.maskrepl=0
#
#     Disable IP source routing.  This could allow attackers to spoof IP addresses that you normally trust as
#     internal hosts.
#
sysctl -w net.inet.ip.sourceroute=0
sysctl -w net.inet.ip.accept_sourceroute=0
#
#
##################################################################
##################################################################
#
#      The sysctl.conf file allows you to configure various aspects of a FreeBSD computer. This includes many
#      advanced options of the TCP/IP stack and virtual memory system that can dramatically improve
#      performance.
#
#      Prevent users from seeing information about processes that are being run under another UID.
#
echo 'security.bsd.see_other_uids=0' >> /etc/sysctl.conf
#
#      Generate a random ID for the IP packets as opposed to incrementing them by one.
#
echo 'net.inet.ip.random_id=1' >> /etc/sysctl.conf
#
#      This will discover dead connections and clear them.
#
echo 'net.inet.tcp.always_keepalive=1' >> /etc/sysctl.conf
#
#      Enabling blackholes for udp and tcp will drop all packets that are received on a closed port and will not
#      give a reply.
#
echo 'net.inet.tcp.blackhole=2' >> /etc/sysctl.conf
echo 'net.inet.udp.blackhole=1' >> /etc/sysctl.conf
#      Disable portmap if you are not running Network File Systems.
echo 'security.bsd.see_other_uids=0' >> /etc/sysctl.conf
echo 'security.bsd.see_other_gids=0'  >> /etc/sysctl.conf
echo 'security.bsd.unprivileged_read_msgbuf=0 '  >> /etc/sysctl.conf'
echo 'security.bsd.unprivileged_proc_debug=0'  >> /etc/sysctl.conf
echo 'kern.randompid=$(jot -r 1 9999)'  >> /etc/sysctl.conf'
echo 'security.bsd.stack_guard_page=1'  >> /etc/sysctl.conf
echo 'security.bsd.see_other_uids=0' >> /etc/sysctl.conf
#
#      Generate a random ID for the IP packets as opposed to incrementing them by one.
#
echo 'net.inet.ip.random_id=1' >> /etc/sysctl.conf
#
#      This will discover dead connections and clear them.
#
echo 'net.inet.tcp.always_keepalive=1' >> /etc/sysctl.conf

echo 'portmap_enable="NO"' >> /etc/rc.conf
#
#      Disable computer system details from being added to /etc/motd on system reboot.
#
echo 'update_motd="NO"' >> /etc/rc.conf
#
#      The /tmp directory should be cleared at startup to ensure that any malicious code that may have
#      entered into the temp file is removed.
#
echo 'clear_tmp_enable="YES"' >> /etc/rc.conf

echo 'security.bsd.see_other_uids=0' >> /etc/sysctl.conf


 edit /etc/ttys and replace every instance of secure with insecure like so:

ttyv0 "/usr/libexec/getty Pc"             xterm       on        insecure
...
dcons "/usr/libexec/getty std.9600"       vt100       off       insecure

pkg install aide
aide --init
cd /var/db/aide/databases
mv aide.db.new aide.db



